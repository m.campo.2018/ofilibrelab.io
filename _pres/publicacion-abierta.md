---
title: Publicación Abierta
date: 2019-11-25
image:
  feature: Publicacion_Abierta.png
  teaser: Publicacion_Abierta.png
transpas:
  - format: PDF
    file: Publicacion_Abierta.pdf
  - format: ODP (LibreOffice)
    file: Publicacion_Abierta.odp
extras:
  - name: Ficha del taller sobre publicación abierta que tuvo lugar en los Talleres de Innovación Educativa y Cultura Abierta, URJC campus de Alcorcón, 25 de noviembre de 2019.
    url: ../transpas/publicacion-abierta/publicacion-abierta-jornadas-innovacion-2019.pdf
---

Introducción a la publicación abierta, desde un punto de vista lo más práctico posible.
