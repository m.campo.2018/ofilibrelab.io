---
title: Otras oficinas Libres
logo: logo-ofilibre.png
date: 2019-10-14
---
A continuación presentamos un listado de la oficinas en España que tienen la misma función o parecida a la de la OfiLibre, es un trabajo en progreso, no es definitivo.

| Oficina | ¿activa? |
| --- | --- |
| [Universidad Complutense de Madrid](https://www.ucm.es/oficina-de-software-libre/) | Sí |
| [Universidad de Granada](https://osl.ugr.es/) | ? |
| [Universidad de La Laguna](https://osl.ull.es/) | Sí |
| [Universidad de Zaragoza](https://osluz.unizar.es/) | No (5/2018) |
| [O.S.L. de las Universidades Gallegas](http://osl.cixug.es/software-libre/?__locale=es) | No (2015) |
| [Universidad Miguel Hernandez de Elche](https://oshl.edu.umh.es/) | Sí  |
| [SALE Software Libre en Euskadi](http://www.euskadi.eus/gobierno-vasco/software-libre/inicio/) | Sí |
| [Universidad de Córdoba](https://www.uco.es/aulasoftwarelibre/) | Sí |
| [Universidad de Cádiz](https://osluca.uca.es/) | Sí |
| [Asociación de universitarios informáticos de Almería](http://asociacion-unia.es/osl-unia/) | No (2016) |
| [Universidad de Huelva](http://petrel.uhu.es/) | No (2014) pero la plataforma funciona |
| [Asociación de SW Libre de la Universidad de Sevilla](https://solfa.us.es/) | Sí |
| [Universidad Autónoma de Barcelona](https://opl.uab.es/) | Sí |
| [Universidad de Las Palmas de Gran Canaria](http://osl.ulpgc.es/quienes-somos/) | No |
| [Universidad Carlos III de Madrid](http://osl.uc3m.es/) | ? |
| [Asociación LibreLab UCM](https://librelabucm.org/) | ? |
| [Universidad de Vigo](http://osl.cixug.es/) | Parece que no |
| [Universidad de Deusto](http://softwarelibre.deusto.es/) | ? |
| [Universidad de Murcia](https://www.um.es/web/softla/) | ? |
